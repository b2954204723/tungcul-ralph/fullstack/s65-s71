import { useState, useEffect, useContext } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'


export default function AddProduct () {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const [ isActive, setIsActive] = useState(false);


  function addProducts (e) {

    
    e.preventDefault();
    const token = localStorage.getItem("token");
    

    fetch(`${process.env.REACT_APP_API_URL}/product/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data) {

        setName("");
        setDescription("");
        setPrice("");

        Swal.fire({
          title: "Product added",
          icon: "success"
        })

        navigate("/product")

      } else {

        Swal.fire({
          title: "Unsuccessful Product Creation",
          icon: "error"
        })
      }

    })

  };

  useEffect(() => {

    if(name !== "" && description !== "" && price !== "") {
        setIsActive(true)
    } else {

        setIsActive(false)
    }

  }, [name, description, price]);

  return (

      (user.id === null || user.isAdmin === false)
      ? navigate('/product')
      :
      <Form onSubmit={e => addProducts(e)} id="addProduct" className="mt-5 p-3">
        <h1 className="my=5 text-center">Add Product</h1>

        <Form.Group className="mb-3" controlId="Course Name">
          <Form.Label>Product:</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="Enter Product" 
            required
            value={name}
            onChange={e => {setName(e.target.value)}}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="Description">
          <Form.Label>Description:</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="Enter Description" 
            required 
            value={description}
            onChange={e => {setDescription(e.target.value)}}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="Price">
          <Form.Label>Price:</Form.Label>
          <Form.Control 
            type="number" 
            placeholder="Enter Price"  
            required
            value={price}
            onChange={e => {setPrice(e.target.value)}}
          />
        </Form.Group>

        {
          isActive 
            ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
            : <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
        }

      </Form>
     

    )
}