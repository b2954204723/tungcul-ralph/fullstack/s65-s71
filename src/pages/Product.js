import {useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import ArchiveProduct from '../components/ArchiveProduct';
import UserContext from '../UserContext';
import { Button, Row, Col } from 'react-bootstrap';
import ProductSearch from '../components/ProductSearch';


export default function Product () {

	const { user } = useContext(UserContext);

	// State that will be used to store product retrieved from the database.
	const [product, setProduct] = useState([]);


	// Create a function to fetch all the product
	const fetchData = () => {

		// use the route /all to get all active and not active courses (make sure that this route at the backend doesn't have jwt)
		//The fetch will be used to pass the data to Userview and Adminview, where:
			//Userview only active courses will be shown
			//Adminview shows all active and non-active courses
		fetch(`${process.env.REACT_APP_API_URL}/product/all`)
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			// // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components.
			// setCourses(data.map(course =>{
			// 	return(
			// 		<CourseCard key={course._id} courseProp={course} />
			// 	)
			// }))
			setProduct(data);
		});
	}




	// Retrieves the courses from the database upon initial render of the "Courses" component
	useEffect(() =>{

		fetchData();

	}, []);


	return(
		<>
			   
			    <h1 className="mt-4" id="productHead">Products</h1>
			  
		    {
		    	(user.isAdmin === true) 
		    		?
		    		<AdminView productData={product} fetchData={fetchData} ArchiveProduct={ArchiveProduct}/>
		    		:
		    		<>
		    		<ProductSearch/>
		    		<UserView productData={product}/>
		    		</>
			}
			
		</>
	)

};
