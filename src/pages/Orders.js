import { useState, useEffect, useContext } from 'react';
import { Container, Table } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Cart() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    // Fetch all orders for the currently logged-in user
    if (user.id) {
      fetch(`${process.env.REACT_APP_API_URL}/order/userOrders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(data => {
          setOrders(data.orders);
          // Calculate the total amount from all orders
          const total = data.orders.reduce((acc, order) => acc + order.totalAmount, 0);
          setTotalAmount(total);
        })
        .catch(error => console.error(error));
    }
  }, [user]);

  return (
    <Container className="mt-5" id="orders">
      <h2 className="text-center p-3">All Orders</h2>
      {orders.length > 0 ? (
        <Table striped bordered responsive>
          <thead>
            <tr>
              <th>Order ID</th>
              <th>Product ID</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Purchased On</th>
              <th>Total Amount</th>
            </tr>
          </thead>
          <tbody>
            {orders.map(order => (
              <tr key={order._id}>
                <td>{order._id}</td>
                <td>{order.products[0].productId}</td>
                <td>{order.products[0].quantity}</td>
                <td>{order.products[0].price}</td>
                <td>{order.purchasedOn}</td>
                <td>{order.totalAmount}</td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan="5" className="text-end fw-bold">Total Amount of all Orders:</td>
              <td>{totalAmount}</td>
            </tr>
          </tfoot>
        </Table>
      ) : (
        <p>No orders found.</p>
      )}
    </Container>
  );
}
