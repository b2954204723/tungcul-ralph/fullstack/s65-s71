import {useState,useEffect, useContext} from 'react';
import {Row, Col} from 'react-bootstrap';
import {  Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import ResetPassword from '../components/ResetPassword';
import UpdateProfile from '../components/UpdateProfile';

export default function Profile(){

    const {user} = useContext(UserContext);

    const [details,setDetails] = useState({})

    const fetchDetails = () => {

      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {

        setDetails(data);

      }
        });

    }

    useEffect(()=>{

        fetchDetails();

    },[])

  return (
        // (user.email === null) ?
        // <Navigate to="/courses" />
        // :

        
        (user.id === null) ?
        <Navigate to="/courses" />
        :
        <> {/*fragments ang tawag sa ganto (//) */}
    <Row>
      <Col className="p-5 mt-5" id="profile">
        <img src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png" alt="profile-pic" width="100" height="100" />
        <h1 className="my-5 ">Profile</h1>
                {/* <h2 className="mt-3">James Dela Cruz</h2> */}
        <h2 className="mt-3">{`${details.firstName} ${details.lastName}`}</h2>
        <hr />
        <h4>Contacts</h4>
        <ul>
          {/* <li>Email: {user.email}</li> */}
                    <li>Email: {details.email}</li>
          {/* <li>Mobile No: 09266772411</li> */}
          <li>Mobile No: {details.mobileNo}</li>
        </ul>
      </Col>
    </Row>

    <Row className="pt-4 mt-4">
      <Col>
        <UpdateProfile fetchDetails={fetchDetails} />
        <ResetPassword />
      </Col>
    </Row>

    </>

  )

}
