import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(); // Default quantity is set to 1

  useEffect(() => {
    // Fetch the product details
    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
      .then(res => res.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value);
    setQuantity(newQuantity);
  };

  const purchase = () => {
    fetch(`${process.env.REACT_APP_API_URL}/order/purchase`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity // Use the selected quantity
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Purchased Successfully',
            icon: 'success',
            text: 'You have Successfully purchased the item'
          });

          navigate('/product');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.'
          });
        }
      });
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
            	<img src="https://i.redd.it/iyj3x6g2tla71.jpg" className="mb-4" alt="profile-pic" width="150" height="100" id="razer"/>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              <div className="d-flex justify-content-center align-items-center">
                <span className="me-2">Quantity:</span>
                <input
                  type="number"
                  min="1"
                  value={quantity}
                  onChange={handleQuantityChange}
                />
              </div>

              {user.id !== null ? (
                <Button onClick={purchase} className="mt-4" id="purchaseButton">Purchase
                </Button>

              ) : (
                <Button as={Link} to="/login" variant="danger" className="mt-4">
                  Log in to Purchase
                </Button>
              )}
              <Button as={Link} to="/product" variant="danger" className="mt-4 mx-3" id="cancelButton">Cancel
                </Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}



/* import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {


	// The useParams hook allows us to retrieve tge courseId passed via the URL
	const {productId} = useParams()

	const { user } = useContext(UserContext);

	// an object with methods to redirect the user
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(() => {
		console.log(productId)

		// a fetch request that will retrieve the details of a specific course
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	const purchase = (productId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/order/purchase`, {
			method: "POST",
			headers: {
		  	"Content-Type": "application/json",
		  	Authorization: `Bearer ${ localStorage.getItem('token') }`
		},
			body: JSON.stringify({
		  	productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

		console.log(data);

		if(data) {
			Swal.fire({
				title: "Purchased Successfully",
				icon: "success",
				text: "You have Successfully purhased the item"
			})

			// allow us to navigate the user back to the course page programmically instead of using component.
			navigate("/product")
		} else {
			Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
		}

		});
	}

	return(
		
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{user.id !== null ?
								<Button variant="primary" onClick={() => purchase(productId)}>Add to cart</Button>
							:
								<Button as={Link} to="/login" variant="danger">Log in to Purchase</Button>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		
	)
} */