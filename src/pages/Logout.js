import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {

	// The ".clear" will delete the token stored in the local storage.
	// localStorage.clear();


	const { unsetUser, setUser } = useContext(UserContext);

	// This invokes the unsetUser function from App.js to clear the data/token from the local storage. This will result to the value undefined.
	unsetUser();

	useEffect(() => {
		// set the user state back to its original value.
		setUser({
			id: null,
			isAdmin: null
		})
	})

	return (

		
		  <Navigate to="/login" />
		
	)
}