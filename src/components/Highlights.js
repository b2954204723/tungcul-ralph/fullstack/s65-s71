import {Row, Col, Card} from 'react-bootstrap';
import '../App.css';


export default function Highlights() {

	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" id="card">
				  <Card.Body>
				    <Card.Title>You can shop where you are</Card.Title>
				    <Card.Text>
				      Discover the freedom to shop wherever your heart desires! Embrace the joy of finding what you love, no matter where you are. Happy shopping!
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" id="cardTwo">
				  <Card.Body>
				    <Card.Title>Quality Products</Card.Title>
				    <Card.Text>
				      Quality products, guaranteed. Elevate your standards and experience excellence with our handpicked selection. Shop with confidence, satisfaction awaits!
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" id="cardThree">
				  <Card.Body>
				    <Card.Title>Let's talk about your upgrades</Card.Title>
				    <Card.Text>
				      Ready for an upgrade? Let's talk! Discover the latest enhancements to elevate your life and experiences. Get started now!
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}







