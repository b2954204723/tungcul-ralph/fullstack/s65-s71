import Table from 'react-bootstrap/Table';
import { useState, useEffect } from 'react';
import { Col, Row, Container, Button } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import { useNavigate } from 'react-router-dom'; // Import the useNavigate hook

export default function AdminView({ productData, fetchData }) {
  const [product, setProduct] = useState([]);
  const navigate = useNavigate(); // Initialize useNavigate

  useEffect(() => {
    setProduct(productData);
  }, [productData]);

  return (
    <Container id="adminView">
      <h1 className="text-center mt-4">Admin Dashboard</h1>
      	{/* Add New Product Button */}
      	<Row className="text-center mb-3">
      	  <Col>
      	    <Button variant="primary" onClick={() => navigate('/addProduct')} className="mx-2" id="add">
      	      Add New Product
      	    </Button>

      	    <Button variant="success" onClick={() => navigate('/allOrders')} id="show">
      	      Show Total Orders
      	    </Button>
      	  </Col>
      	</Row>
      <Row>
        <Col>
          <Table striped bordered hover responsive>
            <thead>
              <tr className="text-center">
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Availability</th>
                <th colSpan="2">Action</th>
              </tr>
            </thead>
            <tbody>
              {product.map((product) => (
                <tr key={product._id}>
                  <td>{product._id}</td>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td className={product.isActive ? 'text-success' : 'text-danger'}>{product.isActive ? 'Available' : 'Unavailable'}</td>
                  <td>
                  <EditProduct product={product._id} fetchData={fetchData}/>
                  </td>
                  <td>
                  <ArchiveProduct productId={product._id}  isActive={product.isActive} fetchData={fetchData}/>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}





/* import Table from 'react-bootstrap/Table';
import { useState, useEffect } from 'react';
import { Col, Row, Container, Button } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';


export default function AdminView ({productData, fetchData}) {

	const [product, setProduct] = useState([]);

	useEffect(() => {
		setProduct(productData);
	}, [productData])
	
	return (

		<Container id="adminView">
			<h1 className="text-center mt-4">Admin Dashboard</h1>
				<Row>
					<Col>
						<Table striped bordered hover responsive>
						      <thead>
						        <tr className="text-center">
						          <th>ID</th>
						          <th>Name</th>
						          <th>Description</th>
						          <th>Price</th>
						          <th>Availability</th>
						          <th colSpan="2">Action</th>
						        </tr>
						      </thead>
						      <tbody>
						      	{product.map((product) => (
						      			<tr key={product._id}>
						      			    <td>{product._id}</td>
						      			    <td>{product.name}</td>
						      			    <td>{product.description}</td>
						      			    <td>{product.price}</td>
						      			    <td className={product.isActive ? 'text-success' : 'text-danger'}>{product.isActive ? 'Available' : 'Unavailable'}</td>
						      			    <td>
						      			    <EditProduct product={product._id} fetchData={fetchData}/>
						      			    </td>
						      			    <td>
						      			    <ArchiveProduct productId={product._id}  isActive={product.isActive} fetchData={fetchData}/>
						      			    </td>

						      			</tr>

						      	))}
						      		
						      </tbody>
						</Table>

					</Col>
				</Row>
		</Container>

	)
}; */