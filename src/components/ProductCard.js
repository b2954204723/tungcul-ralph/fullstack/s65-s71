import	{ useState, useEffect } from 'react';
import {Row, Col, Button, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard ({productProp}) {

	//console.log(props);
	//console.log(typeof props);
	//console.log(props.courseProp.name)

	//console.log(courseProp);

	// Destructured the courseProp object to access its properties.
	const { _id, name, description, price } = productProp;


	return (

		<Row>
			<Col className="pt-5 container">
				<Card className="cardHighlight p-3" id="productCard">
				      <Card.Body>
				      	<img src="https://i.redd.it/iyj3x6g2tla71.jpg" className="mb-4" alt="profile-pic" width="150" height="100" id="razer"/>
				      	<Card.Title>{name}</Card.Title>
				      	<Card.Subtitle>Description:</Card.Subtitle>
				      	<Card.Text>{description}</Card.Text>
				      	<Card.Subtitle>Price:</Card.Subtitle>
				      	<Card.Text>Php {price}</Card.Text>
				      	<Button as={Link} to={`/product/${_id}`} variant="primary" id="viewDetails">View Details</Button>
				      </Card.Body>
				</Card>
			</Col>
		</Row>

	)
};