import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
//import CourseSearch from './CourseSearch';
//import CoursePrice from './CoursePrice';


export default function UserView({productData}) {

	const [product, setProduct] = useState([])

	useEffect(() => {
		const productArr = productData.map(product => {

			//only render the active courses since the route used is /all from Course.js page
			if(product.isActive === true) {
				return (
					<ProductCard productProp={product} key={product._id}/>
					)
			} else {
				return null;
			}
		})

		//set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
		setProduct(productArr)

	}, [productData])

	return(
		<>
			
			
			{ product }
		</>
	)
}