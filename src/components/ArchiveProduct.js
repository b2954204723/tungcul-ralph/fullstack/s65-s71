import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const ArchiveProduct = ({ productId, isActive, fetchData }) => {
  const handleArchiveToggle = () => {
    // Fetch /archive from the backend
    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        isActive: !isActive
      })
    })

    // Add sweetalert for confirmation
    Swal.fire({
      title: "Success!",
      icon: "success",
      text:"Product successfully disabled",
    })

    // On successful response, fetch updated data using fetchData()
    fetchData();

  };

  const handleActivateToggle = () => {
    // Fetch /activate from the backend
    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}/activate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        isActive: !isActive
      })
    })
    // Add sweetalert for confirmation
    Swal.fire({
      title: "Success!",
      icon: "success",
      text:"Product successfully enabled",
    })
    // On successful response, fetch updated data using fetchData()
    fetchData();

  };

  return (
    <div>
      {isActive ? (
        <Button variant='danger' size="sm" onClick={handleArchiveToggle}>Disable</Button>
      ) : (
        <Button variant='success' size="sm" onClick={handleActivateToggle}>Activate</Button>
      )}
    </div>
  );
}
export default ArchiveProduct;